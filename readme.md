## Jupyter Notebook Server

### launch server

```
    make
```

### gracefully stop server
```
    make stop
```

### restore a notebook from .py version
```
jupytext --to notebook notebook.py              # convert notebook.py to an .ipynb file with no outputs
jupytext --update --to notebook notebook.py     # update the input cells in the .ipynb file and preserve output
```
