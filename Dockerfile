FROM jupyter/base-notebook

USER root

RUN apt-get update && apt-get install -y \
  postgresql-client \
  python3-pandas

USER $NB_UID

RUN pip install --upgrade pip

RUN pip install \
  psycopg2-binary \
  sqlalchemy \
  pandas \
  matplotlib \
  jupytext

