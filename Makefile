launch: build
	docker run -d -p 8888:8888 -v ${CURDIR}/notebooks:/home/jovyan/work --name ipynb_server ipynb start-notebook.sh \
		--NotebookApp.token='' \
		--NotebookApp.password='' 
build:
	docker build -f ${CURDIR}/Dockerfile -t ipynb .

shell:
	docker exec -it ipynb_server bash
stop:
	docker stop ipynb_server
	docker rm ipynb_server
