# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Example Notebook

# ### Command Reference
# ```
# # restore jupytext .py files
#
# jupytext --to notebook notebook.py              # convert notebook.py to an .ipynb file with no outputs
# jupytext --update --to notebook notebook.py     # update the input cells in the .ipynb file and preserve outputs and metadata
#
#
# # install library from notebook
#
# # !pip install matplotlib
# ```

# ## DB Connection and Introspection

import sqlalchemy
import pandas as pd
from sqlalchemy import create_engine


# +
def load_config():
    import json
    with open('notebook_config.json') as f:
        config = json.load(f)
    return config

def engine_from_creds(db_creds):
    return create_engine('{wrapper}://{user}:{passwd}@{host}/{db}'.format(**db_creds))

db_creds = load_config()['dbs']['pgdb']
engine = engine_from_creds(db_creds)
# -

tables = pd.read_sql("SELECT * FROM information_schema.tables", engine)
tables.head()

# +
table_name = tables['table_name'][0]
#table_name = 'cards'

columns = pd.read_sql("SELECT * FROM information_schema.columns where table_name = '{table_name}'".format(table_name=table_name), engine)
columns
